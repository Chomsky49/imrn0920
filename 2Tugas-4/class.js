// Soal 1 (Release 0) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// class Animal {
//     constructor(legs, cold_blooded){
//         this.kaki = legs
//         this.cold = cold_blooded
//     }
//     get legs(){
//         this.kaki = 4
//         return this.kaki 
//     }
//     get name(){
//         return "shaun"
//     }
//     get cold_blooded(){
//         return false
// }
// }
// var sheep = new Animal("shaun");
 
// console.log(sheep.name) // "shaun"
// console.log(sheep.legs) // 4
// console.log(sheep.cold_blooded) // false

// Soal 1 (Release 1) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// class Animal {
//     constructor(legs, cold_blooded){
//         this.kaki = legs
//         this.cold = cold_blooded
//     }
// }
// class Ape extends Animal {
//     constructor(legs, yell){
//         super(legs)
//         this.suara = yell
//     }
//     yell(){
//         return console.log("Auoo")
//     }
// }

// class Frog extends Animal{
//     constructor(legs, jump){
//         super(legs)
//         this.lompat = jump
//     }

//     jump(){
//         return console.log("hop hop")
//     }
// }
// var sungokong = new Ape("kera sakti")
// sungokong.yell() // "Auooo"

// var kodok = new Frog("buduk")
// kodok.jump() // "hop hop" 


// Soal 2 Function To Class ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  class Clock {
        constructor({ template }) {
          this.template = template;
        }
        render() {
          let date = new Date();
      
          let hours = date.getHours();
          if (hours < 10) hours = '0' + hours;
      
          let mins = date.getMinutes();
          if (mins < 10) mins = '0' + mins;
      
          let secs = date.getSeconds();
          if (secs < 10) secs = '0' + secs;
      
          let output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
      
          console.log(output);
        }
      
        stop() {
          clearInterval(this.timer);
        }
      
        start() {
          this.render();
          this.timer = setInterval(() => this.render(), 1000);
  }
  }
var clock = new Clock({template: 'h:m:s'});
clock.start(); 