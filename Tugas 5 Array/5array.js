function balikKata(kata){
    var rubah = "";
    for (var i = kata.length-1 ; i>= 0; i--){
        rubah = rubah += kata[i];
    }
    return rubah;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I
