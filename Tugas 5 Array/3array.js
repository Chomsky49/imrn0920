function sum(startnum, finishnum, step){
    var angka = []
    var total = 0
    if (startnum<finishnum){
        if (step == null)
            for (i=startnum; i<= finishnum; i ++){
                angka.push(i);
                total = total + i
        }else{
            for (i=startnum; i <= finishnum; i += step){
                angka.push(i)
                total= total + i
            }
        }
    }else if (startnum>finishnum) {
            if (step == null){
                for (i = startnum; i>= finishnum; i--){
                    angka.push(i)
                    total = total + i
                }
            }else{
                for (i = startnum; i >= finishnum; i -= step ){
                angka.push(i)
                total = total + i}
            }
        
    }else if (finishnum == null) {
            for (i=startnum; i<= finishnum; i ++){
                angka.push(i);
                total = total + i
            }
    }else{
        angka.push(0)
    }
    return total
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 